vim.g.mapleader = " "

local keymap = vim.keymap -- for conciseness

keymap.set("i", "jk", "<ESC>", { desc = "Exit insert mode with jk" })

keymap.set("n", "<leader>nh", ":nohl<CR>", { desc = "Clear search highlights" })

-- Normal mode
keymap.set("n", "<leader>/", function()
  require("Comment.api").toggle.linewise.current()
end, { desc = "Toggle comment" })

-- Visual mode
keymap.set(
  "v",
  "<leader>/",
  "<ESC><cmd>lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<CR>",
  { desc = "Toggle comment" }
)

-- Terminal mode
keymap.set("t", "<A-i>", function()
  require("nvterm.terminal").toggle("float")
end, { desc = "Toggle floating term" })

keymap.set("t", "<A-h>", function()
  require("nvterm.terminal").toggle("horizontal")
end, { desc = "Toggle horizontal term" })

keymap.set("t", "<A-v>", function()
  require("nvterm.terminal").toggle("vertical")
end, { desc = "Toggle vertical term" })

-- Normal mode
keymap.set("n", "<A-i>", function()
  require("nvterm.terminal").toggle("float")
end, { desc = "Toggle floating term" })

keymap.set("n", "<A-h>", function()
  require("nvterm.terminal").toggle("horizontal")
end, { desc = "Toggle horizontal term" })

keymap.set("n", "<A-v>", function()
  require("nvterm.terminal").toggle("vertical")
end, { desc = "Toggle vertical term" })

keymap.set("n", "<leader>h", function()
  require("nvterm.terminal").new("horizontal")
end, { desc = "New horizontal term" })

keymap.set("n", "<leader>v", function()
  require("nvterm.terminal").new("vertical")
end, { desc = "New vertical term" })

-- Signature
keymap.set("n", "<leader>k", function()
  require("lsp_signature").toggle_float_win()
end, { silent = true, noremap = true, desc = "toggle signature" })
-- keymap.set("n", "<leader>k", function()
--   vim.lsp.buf.signature_help()
-- end, { silent = true, noremap = true, desc = "toggle signature" })

-- increment/decrement numbers
keymap.set("n", "<leader>+", "<C-a>", { desc = "Increment number" }) -- increment
keymap.set("n", "<leader>-", "<C-x>", { desc = "Decrement number" }) -- decrement

-- window management
keymap.set("n", "<leader>sv", "<C-w>v", { desc = "Split window vertically" }) -- split window vertically
keymap.set("n", "<leader>sh", "<C-w>s", { desc = "Split window horizontally" }) -- split window horizontally
keymap.set("n", "<leader>se", "<C-w>=", { desc = "Make splits equal size" }) -- make split windows equal width & height
keymap.set("n", "<leader>sx", "<cmd>close<CR>", { desc = "Close current split" }) -- close current split window

keymap.set("n", "<leader>to", "<cmd>tabnew<CR>", { desc = "Open new tab" }) -- open new tab
keymap.set("n", "<leader>tx", "<cmd>tabclose<CR>", { desc = "Close current tab" }) -- close current tab
keymap.set("n", "<leader>tn", "<cmd>tabn<CR>", { desc = "Go to next tab" }) --  go to next tab
keymap.set("n", "<leader>tp", "<cmd>tabp<CR>", { desc = "Go to previous tab" }) --  go to previous tab
keymap.set("n", "<leader>tf", "<cmd>tabnew %<CR>", { desc = "Open current buffer in new tab" }) --  move current buffer to new tab
