return {
  "akinsho/bufferline.nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  version = "*",
  opts = {
    -- options = {
    --   mode = "tabs",
    -- },
    options = {
      mode = "buffers",
      separator = true,
    },
  },
}
